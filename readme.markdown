Next DART
=========

A Pebble app for checking DART times. It uses [Irish Rail’s realtime API](http://api.irishrail.ie/realtime/).

It’s a [PebbleKit JavaScript](https://developer.getpebble.com/2/guides/javascript-guide.html) app, which slightly complicates how it can be deployed (for iOS users at least). Essentially, the app is tied to the Pebble appstore. From the [Pebble appstore FAQ](http://developer.getpebble.com/2/distribute/publish-to-pebble-appstore.html):

> ### How will JavaScript apps be distributed to iOS users?
> 
> The Pebble iOS application cannot execute code downloaded from the Internet so we will package the JavaScript code of all Pebble appstore apps and include them in the official Pebble iOS app.

Requirements to build
--

+ Install the [Pebble SDK](https://developer.getpebble.com/sdk/download/?download=true)

Licence
--

MIT