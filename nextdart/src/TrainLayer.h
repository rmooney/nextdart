// forward declarations
typedef struct Layer Layer;
typedef struct TextLayer TextLayer;
typedef struct GRect GRect;
typedef struct Train Train;


typedef struct TrainLayer {
    Layer *root_layer;
    TextLayer *due_text_layer;
    TextLayer *destination_text_layer;
    Train *train;
} TrainLayer;

// Train layers are a set size (124 x 24). The size information in the frame argument is ignored.
TrainLayer *train_layer_create(GRect frame);

void train_layer_destroy(TrainLayer *train_layer);

void train_layer_set_train(TrainLayer *train_layer, Train *train);

Layer *train_layer_get_layer(TrainLayer *train_layer);