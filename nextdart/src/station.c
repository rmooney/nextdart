#include <pebble.h>
#include "station.h"

static void uppercase(char *str)
{
	char *c = str;
	char offset = 'a' - 'A';
	
	while (*c != '\0') {
		if (*c >= 'a' && *c <= 'z') {
			*c -= offset;
		}
		c++;
	}
}

Station *station_create(const char *name)
{
    Station *station = malloc(sizeof(Station));
    
    size_t len = strlen(name) + 1;
    char uppercase_name[sizeof(char) * len];
    strncpy(uppercase_name, name, len);
    uppercase(uppercase_name);
    
    station->name = malloc(sizeof(char) * len);
    strncpy(station->name, uppercase_name, len);
    
    return station;
}

void station_destroy(Station *station)
{
    if (!station) {
        return;
    }
    free(station->name);
    free(station);
}