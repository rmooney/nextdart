#include <pebble.h>

#ifdef PBL_COLOR
    #define FOREGROUND_COLOR GColorWhite
    #define BACKGROUND_COLOR GColorDarkGreen
    #define HEADER_FOREGROUND_COLOR GColorDarkGreen
    #define HEADER_BACKGROUND_COLOR GColorKellyGreen
#else
    #define FOREGROUND_COLOR GColorWhite
    #define BACKGROUND_COLOR GColorBlack
    #define HEADER_FOREGROUND_COLOR GColorBlack
    #define HEADER_BACKGROUND_COLOR GColorWhite
#endif