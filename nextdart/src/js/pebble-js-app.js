function SuperSimpleXMLParser(onTagStart, onTextChunk, onTagEnd) {    
    "use strict";

    this.onTagStart = onTagStart;
    this.onTextChunk = onTextChunk;
    this.onTagEnd = onTagEnd;

    this.ignoreTagMode = false;
    this.tagMode = false;
    this.startTagMode = false;
    this.attributeKeyMode = false;
    this.attributeValueMode = false;
    this.attributeEscapeMode = false;
    this.endTagMode = false;
    this.escapeMode = false;
    this.emptyTagMode = false;

    this.tagName = "";
    this.textChunk = "";
                    
    this.entityChunk = "";
    this.attributes = undefined;
    this.attributeKey = "";
    this.attributeValue = "";
    
    this.cancelled = false;
}

SuperSimpleXMLParser.predefinedEntities = { 
    "lt" : 60, 
    "gt" : 62, 
    "amp" : 38, 
    "apos" : 39, 
    "quot" : 34 
};

SuperSimpleXMLParser.prototype.parseChunk = function (chunk) {
    "use strict";

    var c, i, entity;
    for (i = 0; i < chunk.length; i++) {
        if (this.cancelled) {
            break;
        }
        c = chunk.charAt(i);
        if (this.ignoreTagMode) {
            if (c === ">") {
                this.ignoreTagMode = false; 
                this.tagMode = false; 
            }
            continue;  
        }
    
        if (this.attributeEscapeMode) {
            this.attributeEscapeMode = false;
            this.attributeValue += c;
            continue;
        }
    
        if (this.escapeMode) {
            if (c === ";") {
                this.escapeMode = false;
                entity = SuperSimpleXMLParser.predefinedEntities[this.entityChunk]; 
                if (!entity) {
                    if (this.entityChunk.indexOf("#x") === 0) {
                        entity = parseInt("0" + this.entityChunk.slice(1));
                    } else {
                        entity = parseInt(this.entityChunk.slice(1));                        
                    }
                }
                if (entity > 31) {
                    if (this.attributeValueMode) {
                        this.attributeValue += String.fromCharCode(entity);
                    } else {
                        this.textChunk += String.fromCharCode(entity);
                    }
                }
                this.entityChunk = "";
            } else {
                this.entityChunk += c;
            }
            continue;          
        }
    
        if (this.tagMode) {
            this.tagMode = false; 
            if (c === "/") {
                this.endTagMode = true;
                continue;
            } else if (c == "?" || c == "!") {
                this.ignoreTagMode = true;
                continue;
            } else {
                this.startTagMode = true;            
            }
        }
    
        if (this.startTagMode) {
            if (c === ">") {
                this.startTagMode = false;                
                this.tagName = this.tagName.trim();
                if (this.onTagStart) {
                    this.onTagStart(this.tagName, this.attributes);
                }                
                if (this.emptyTagMode && this.onTagEnd) {
                    this.emptyTagMode = false;
                    this.onTagEnd(this.tagName);
                }
                this.tagName = "";
                this.attributes = undefined;
            } else if (c === " ") {
                this.attributeKeyMode = true;
                this.startTagMode = false;
            } else {
                this.tagName += c;
            }
            continue;
        }  
    
        if (this.attributeKeyMode) { 
            if (c === "\"") {
                this.attributeKey = this.attributeKey.trim();
                this.attributeKeyMode = false;          
                this.attributeValueMode = true; 
            } else if (c === "/") {
                this.attributeKeyMode = false;
                this.emptyTagMode = true;
                this.startTagMode = true;
            } else if (c !== "=") {                         
                this.attributeKey += c;
            }
            continue;
        } 
    
        if (this.attributeValueMode) {
            if (c === "&") {
                this.escapeMode = true;
            } else if (c === "\\") {
                this.attributeEscapeMode = true;
            } else if (c === "\"") {    
                this.attributeValue = this.attributeValue.trim();
                this.attributeValueMode = false; 
                if (!this.attributes) {
                    this.attributes = {};
                }
                this.attributes[this.attributeKey] = this.attributeValue;
                this.attributeKey = this.attributeValue = "";
                this.startTagMode = true;  
            } else {                         
                this.attributeValue += c;
            }
            continue;
        } 
         
        if (this.endTagMode) {
            if (c === ">") {
                this.endTagMode = false; 
                if (this.onTagEnd) {
                    this.onTagEnd(this.tagName);
                }
                this.tagName = "";
            } else {
                this.tagName += c;
            }
            continue;
        }
    
        if (c === "<") {
            this.textChunk = this.textChunk.trim();
            if (this.textChunk.length) {
                if (this.onTextChunk) {
                    this.onTextChunk(this.textChunk);
                }
                this.textChunk = "";
            }
            this.tagMode = true; 
            continue;       
        }
    
        if (c === "&") {
            this.escapeMode = true;
            continue;       
        }
            
        this.textChunk += c;
    }
};

SuperSimpleXMLParser.prototype.cancel = function () {
    this.cancelled = true;
};

var App,
    Message,
    Train,
    Station,
    ServerError,
    stations;

App = {};
App.fetchTrainData = function () {
    "use strict";
    navigator.geolocation.getCurrentPosition(function (position) {
        var nearestStation = Station.nearestStationToCoordinate(stations, position.coords);
        nearestStation.downloadTrains(function (trains, error) {
            var northboundTrains,
                southboundTrains,
                northboundTrain,
                southboundTrain,
                northboundTrainsByteArray = [],
                southboundTrainsByteArray = [],
                i,
                message;

            if (!error) {
                northboundTrains = trains.filter(function (train) { return train.direction === Train.DIRECTION_NORTHBOUND; });
                southboundTrains = trains.filter(function (train) { return train.direction === Train.DIRECTION_SOUTHBOUND; });
                for (i = 0; i < 3; i += 1) {
                    northboundTrain = northboundTrains[i];
                    southboundTrain = southboundTrains[i];
                    if (northboundTrain) {
                        northboundTrainsByteArray.push(northboundTrain.due);
                        northboundTrainsByteArray.push(northboundTrain.destination);
                        northboundTrainsByteArray.push(0);
                    }

                    if (southboundTrain) {
                        southboundTrainsByteArray.push(southboundTrain.due);
                        southboundTrainsByteArray.push(southboundTrain.destination);
                        southboundTrainsByteArray.push(0);
                    }
                }

                message = {
                    status: Message.STATUS_OK,
                    station: this.name,
                    northboundTrains: northboundTrainsByteArray,
                    southboundTrains: southboundTrainsByteArray
                };
            } else {
                if (ServerError.isPrototypeOf(error)) {
                    message = { status: Message.STATUS_SERVER_ERROR };
                } else {
                    message = { status: Message.STATUS_NO_INTERNET };
                }
            }

            Pebble.sendAppMessage(message);
        });
    }, function () {
        Pebble.sendAppMessage({ status: Message.STATUS_NO_LOCATION });
    }, { 
        maximumAge: 600000, 
        timeout: 600000, 
        enableHighAccuracy: false
    });
};

ServerError = Object.create(Error.prototype);
ServerError.name = "";
ServerError.message = "";

ServerError.create = function (message) {
    "use strict";
    return Object.create(this, {
        message : { value : message }
    });
};

Message = {};
Message.STATUS_OK = 0;
Message.STATUS_NO_INTERNET = 1;
Message.STATUS_SERVER_ERROR = 2;
Message.STATUS_NO_LOCATION = 3;

Train = {};
Train.DIRECTION_NORTHBOUND = 0;
Train.DIRECTION_SOUTHBOUND = 1;
Train.destination = "";
Train.due = 0;
Train.direction = 0;

Train.description = function () {
    "use strict";
    var description = "Destination: ";

    description += this.destination;
    description += " Due: ";
    description += this.due + (this.due === 1 ? " min" : " mins");

    description += " Direction: ";
    description += this.direction ? "Southbound" : "Northbound";

    return description;
};

Station = {};
Station.name = "";
Station.code = "";
Station.latitude = 0;
Station.longitude = 0;

Station.create = function (name, code, coordinate) {
    "use strict";
    return Object.create(this, {
        name : { value : name },
        code : { value : code },
        latitude : { value : coordinate.latitude },
        longitude : { value : coordinate.longitude }
    });
};

Station.downloadTrains = function (callback) {
    "use strict";
    var request = new XMLHttpRequest(),
        trains = [],
        station = this;
    request.open("GET", "https://api.irishrail.ie/realtime/realtime.asmx/getStationDataByCodeXML?StationCode=" + this.code, true);
    
    request.onload = function () {
        var parser,
            currentTag,
            currentTrain,
            northboundTrainCount = 0,
            southboundTrainCount = 0;
            

        if (request.readyState === 4) {
            if (request.status === 200) {
                parser = new SuperSimpleXMLParser();

                parser.onTagStart = function (tag) {
                    currentTag = tag;

                    if (tag === "objStationData") {
                        currentTrain = Object.create(Train);
                    }
                };

                parser.onTagEnd = function (tag) {
                    currentTag = null;
                    if (tag === "objStationData") {
                        trains.push(currentTrain);
                        if (currentTrain.direction === Train.DIRECTION_NORTHBOUND) {
                            northboundTrainCount++;
                        } else {
                            southboundTrainCount++;
                        }
                        
                        if (northboundTrainCount > 2 && southboundTrainCount > 2) {
                            // we have enough trains
                            parser.cancel();
                        }
                        currentTrain = null;
                    }
                };

                parser.onTextChunk = function (text) {
                    switch (currentTag) {
                    case "Destination":
                        currentTrain.destination = text;
                        break;
                    case "Duein":
                        currentTrain.due = parseInt(text, 10);
                        break;
                    case "Direction":
                        currentTrain.direction = (text === "Northbound") ? Train.DIRECTION_NORTHBOUND : Train.DIRECTION_SOUTHBOUND;
                        break;
                    }
                };

                parser.parseChunk(request.response);

                callback.apply(station, [trains]);

            } else { // Non 200 OK response
                callback.apply(station, [null, ServerError.create("Server Error " + request.status)]);
            }
        }
    };

    request.onerror = function (error) {
        "use strict";
        callback.apply(station, [null, error]);
    };

    request.send(null);
};

Station.distanceFromCoordinate = function (coordinate) {
    "use strict";
    function degreesToRadians(degrees) {
        return degrees * (Math.PI / 180);
    }

    var RadiusOfEarth = 6371, // km
        latitudeDelta = degreesToRadians(coordinate.latitude - this.latitude),
        longitudeDelta = degreesToRadians(coordinate.longitude - this.longitude),
        a = Math.sin(latitudeDelta / 2) * Math.sin(latitudeDelta / 2) + Math.sin(longitudeDelta / 2) * Math.sin(longitudeDelta / 2) * Math.cos(degreesToRadians(this.latitude)) * Math.cos(degreesToRadians(this.latitude)),
        c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

    return RadiusOfEarth * c;
};

Station.nearestStationToCoordinate = function (stations, coordinate) {  
    "use strict";      
    var nearestStation,
        shortestDistance = Number.MAX_VALUE,
        station,
        distance,
        i,
        numberOfStations = stations.length;

    for (i = 0; i < numberOfStations; i += 1) {
        station = stations[i];
        distance = station.distanceFromCoordinate(coordinate);

        if (distance < shortestDistance) {
            shortestDistance = distance;
            nearestStation = station;
        }
    }

    return nearestStation;
};

stations = [
	Station.create("Ashtown", "ASHTN", {latitude: 53.3755, longitude: -6.33135}),
	Station.create("Balbriggan", "BBRGN", {latitude: 53.6118, longitude: -6.18226}),
	Station.create("Bayside", "BYSDE", {latitude: 53.3917, longitude: -6.13678}),
	Station.create("Blackrock", "BROCK", {latitude: 53.3027, longitude: -6.17833}),
	Station.create("Booterstown", "BTSTN", {latitude: 53.3099, longitude: -6.19498}),
	Station.create("Bray", "BRAY", {latitude: 53.2043, longitude: -6.10046}),
	Station.create("Broombridge", "BBRDG", {latitude: 53.3725, longitude: -6.29869}),
	Station.create("Castleknock", "CNOCK", {latitude: 53.3816, longitude: -6.37149}),
	Station.create("Clondalkin", "CLDKN", {latitude: 53.3334, longitude: -6.40628}),
	Station.create("Clondalkin", "CLONF", {latitude: 53.3334, longitude: -6.40628}),
	Station.create("Clongriffin", "GRGRD", {latitude: 53.4032, longitude: -6.14839}),
	Station.create("Clonsilla", "CLSLA", {latitude: 53.3831, longitude: -6.4242}),
	Station.create("Clontarf Road", "CTARF", {latitude: 53.3629, longitude: -6.22753}),
	Station.create("Coolmine", "CMINE", {latitude: 53.3776, longitude: -6.39072}),
	Station.create("Dalkey", "DLKEY", {latitude: 53.2756, longitude: -6.10333}),
	Station.create("Docklands", "DCKLS", {latitude: 53.3509, longitude: -6.23929}),
	Station.create("Donabate", "DBATE", {latitude: 53.4855, longitude: -6.15134}),
	Station.create("Drumcondra", "DCDRA", {latitude: 53.3632, longitude: -6.25908}),
	Station.create("Dublin Connolly", "CNLLY", {latitude: 53.3531, longitude: -6.24591}),
	Station.create("Dublin Heuston", "HSTON", {latitude: 53.3464, longitude: -6.29461}),
	Station.create("Dublin Pearse", "PERSE", {latitude: 53.3433, longitude: -6.24829}),
	Station.create("Dun Laoghaire", "DLERY", {latitude: 53.2951, longitude: -6.13498}),
	Station.create("Dunboyne", "DBYNE", {latitude: 53.4175, longitude: -6.46483}),
	Station.create("Glenageary", "GLGRY", {latitude: 53.2812, longitude: -6.12289}),
	Station.create("Grand Canal Dock", "GCDK", {latitude: 53.3397, longitude: -6.23773}),
	Station.create("Greystones", "GSTNS", {latitude: 53.1442, longitude: -6.06085}),
	Station.create("Hansfield", "HAFLD", {latitude: 53.3853, longitude: -6.44205}),
	Station.create("Harmonstown", "HTOWN", {latitude: 53.3786, longitude: -6.19131}),
	Station.create("Howth Junction", "HWTHJ", {latitude: 53.3909, longitude: -6.15672}),
	Station.create("Howth", "HOWTH", {latitude: 53.3891, longitude: -6.07401}),
	Station.create("Kilbarrack", "KBRCK", {latitude: 53.387, longitude: -6.16163}),
	Station.create("Kilcock", "KCOCK", {latitude: 53.4043, longitude: -6.67892}),
	Station.create("Kilcoole", "KCOOL", {latitude: 53.107, longitude: -6.04112}),
	Station.create("Killester", "KLSTR", {latitude: 53.373, longitude: -6.20442}),
	Station.create("Killiney", "KILNY", {latitude: 53.2557, longitude: -6.11317}),
	Station.create("Lansdowne Road", "LDWNE", {latitude: 53.3347, longitude: -6.22979}),
	Station.create("Leixlip (Confey)", "LXCON", {latitude: 53.3743, longitude: -6.48624}),
	Station.create("Leixlip (Louisa Br)", "LXLSA", {latitude: 53.3704, longitude: -6.50598}),
	Station.create("M3 Parkway", "M3WAY", {latitude: 53.4349, longitude: -6.46898}),
	Station.create("Malahide", "MHIDE", {latitude: 53.4509, longitude: -6.15649}),
	Station.create("Navan Rd Parkway", "PHNPK", {latitude: 53.3777, longitude: -6.34591}),
	Station.create("Portmarnock", "PMNCK", {latitude: 53.4169, longitude: -6.1512}),
	Station.create("Raheny", "RAHNY", {latitude: 53.3815, longitude: -6.17699}),
	Station.create("Rush and Lusk", "RLUSK", {latitude: 53.5201, longitude: -6.1439}),
	Station.create("Salthill", "SHILL", {latitude: 53.2954, longitude: -6.15206}),
	Station.create("Sandycove", "SCOVE", {latitude: 53.2878, longitude: -6.12712}),
	Station.create("Sandymount", "SMONT", {latitude: 53.3281, longitude: -6.22116}),
	Station.create("Seapoint", "SEAPT", {latitude: 53.2991, longitude: -6.16512}),
	Station.create("Shankill", "SKILL", {latitude: 53.2364, longitude: -6.11691}),
	Station.create("Skerries", "SKRES", {latitude: 53.5741, longitude: -6.11933}),
	Station.create("Sutton", "SUTTN", {latitude: 53.392, longitude: -6.11448}),
	Station.create("Sydney Parade", "SIDNY", {latitude: 53.3206, longitude: -6.21112}),
	Station.create("Tara Street", "TARA", {latitude: 53.347, longitude: -6.25425}),
];

Pebble.addEventListener("appmessage", function () {
    "use strict";
    App.fetchTrainData();
});

Pebble.addEventListener("ready", function () {
    "use strict";
    App.fetchTrainData();
});

