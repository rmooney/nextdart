#import <Foundation/Foundation.h>

@interface MyParserDelegate : NSObject <NSXMLParserDelegate>

@property (strong, nonatomic) NSMutableArray *result;
@property (strong, nonatomic) NSMutableDictionary *entry;
@property (strong, nonatomic) NSMutableString *text;

@end

@implementation MyParserDelegate

- (void)parserDidStartDocument:(NSXMLParser *)parser
{	
	self.result = [NSMutableArray array];
}

-  (void)parser:(NSXMLParser *)parser 
didStartElement:(NSString *)elementName 
   namespaceURI:(NSString *)namespaceURI 
  qualifiedName:(NSString *)qualifiedName 
     attributes:(NSDictionary *)attributeDict
{
	if ([elementName isEqualToString:@"objStation"]) {
		self.entry = [NSMutableDictionary dictionary];
		self.entry[@"coordinate"] = [NSMutableDictionary dictionary];
	}
	
	self.text = [NSMutableString string];
}

-  (void)parser:(NSXMLParser *)parser 
foundCharacters:(NSString *)string
{
	[self.text appendString:string];
}

- (void)parser:(NSXMLParser *)parser 
 didEndElement:(NSString *)elementName 
  namespaceURI:(NSString *)namespaceURI 
 qualifiedName:(NSString *)qName
{	
	if ([elementName isEqualToString:@"objStation"]) {
		[self.result addObject:self.entry];
		self.entry = nil;
	}
	
	if ([elementName isEqualToString:@"StationDesc"]) {
		self.entry[@"name"] = self.text;
	}
	
	if ([elementName isEqualToString:@"StationCode"]) {
		self.entry[@"code"] = [self.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
	}
	
	if ([elementName isEqualToString:@"StationLatitude"]) {
		self.entry[@"coordinate"][@"latitude"] = @([self.text doubleValue]);
	}
	
	if ([elementName isEqualToString:@"StationLongitude"]) {
		self.entry[@"coordinate"][@"longitude"] = @([self.text doubleValue]);
	}
	
	self.text = nil;
}

@end

int main(int argc, char *argv[]) {
	@autoreleasepool {
		NSData *stationXMLData = [NSData dataWithContentsOfURL:[NSURL URLWithString:@"http://api.irishrail.ie/realtime/realtime.asmx/getAllStationsXML_WithStationType?StationType=D"]];
		
		MyParserDelegate *parserDelegate = [MyParserDelegate new];
		
		NSXMLParser *parser = [[NSXMLParser alloc] initWithData:stationXMLData];
		parser.delegate = parserDelegate;
		[parser parse];
		
		for (NSDictionary *station in parserDelegate.result) {
			NSString *statement = [NSString stringWithFormat:@"Station.createStation(\"%@\", \"%@\", {latitude: %@, longitude: %@}),", station[@"name"], station[@"code"], station[@"coordinate"][@"latitude"], station[@"coordinate"][@"longitude"]];
			puts(statement.UTF8String);
		}
	}
}